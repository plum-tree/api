# Plum Tree API

> Wait, I'm Having One Of Those Things, You Know, A Headache With Pictures.

Repo for the plum tree backend (API).

Written in [NodeJS][nodejs] and deployed to [Lambda functions][lambda] +
[API Gateway][apigateway] using the [Serverless framework][serverless].

## Running

### Prerequisites

#### AWS Infrastructure

The API code assumes you've used the [infrastructure][infrastructure] code to
provision the underlying AWS infrastructure. It also assumes you've configured
your AWS credentials to be able access this.

The underlying infrastructure at a high level includes:

- AWS Cognito for Auth.
- DynamoDB as a database.
- S3 to store uploaded images.
- API Gateway to attach API endpoints code to.

#### Expected Environment Variables

> 📍 Tip: use a `.env` file to set these variables

| Name | Description | Example Value |
| ---- | ----------- | ------------- |
| `STATE_BUCKET` | S3 bucket name where serverless stores state | `plum-tree-state` |
| `DOMAIN` | The plum trees domain you're using | `theplumtreeapp.com` |
| `AWS_ACCOUNT_ID` | AWS account Id you're using | 123456789000 |
| `UPLOAD_BUCKET_INPUT` | S3 bucket name where uploads are first saved to be processed, images here are deleted later using life cycles | `uploads-input` |
| `UPLOAD_BUCKET_PROCESSED` | S3 bucket name where processed uploaded images end up | `uploads-processed` |
| `STACK` | Color or name of stack to deploy to | `blue` |
| `API_GATEWAY_ID` | The API Gateway | `foobar1234` |
| `API_ROOT_RESOURCE_ID` | Root resource of the API Gateway | `foobar1234` |
| `CLIENT_ID` | Client Id from AWS Cognito | `foobar1234` |
| `CLIENT_SECRET` | Client secret from AWS Cognito | `foobar1234` |
| `COGNITO_POOL_ID` | User pool Id from AWS Cognito | `eu-west-1_foobar1234` |
| `CODE_VERIFIER` | OIDC Code verifier | `foobar1234` |
| `JWKS` | Cognito JWKs JSON Object | `{"keys":[...]}` |

Most of the above should be fairly self explanatory to find. Though for the JWKs
this isn't in the AWS console for AWS Cognito so instead go to
`https://cognito-idp.eu-west-1.amazonaws.com/<USER_POOL_ID>/.well-known/jwks.json`.
be sure to replace `<USER_POOL_ID>` with the user pool Id you got when
provisioning the infrastructure.

### Running

Run `npm start` to run locally.

[infrastructure]: https://gitlab.com/plum-tree/infrastructure
[nodejs]: https://nodejs.org/en/
[lambda]: https://aws.amazon.com/lambda/
[apigateway]: https://aws.amazon.com/api-gateway/
[serverless]: https://www.serverless.com/framework/docs/
[bluegreen]: https://martinfowler.com/bliki/BlueGreenDeployment.html
