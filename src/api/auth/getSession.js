const { getCookie } = require('../../helpers/cookies')

exports.handler = async (event) => {
  const token = getCookie(event, 'AuthToken')

  return {
    statusCode: 200,
    body: JSON.stringify({
      token
    })
  }
}
