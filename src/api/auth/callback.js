const qs = require('querystring')
const get = require('lodash.get')
const { Issuer } = require('openid-client')
const log = require('../../helpers/log')
const getHeaders = require('../../helpers/getHeaders')
const { getCookie, setCookie } = require('../../helpers/cookies')
const getCallbackUri = require('../../helpers/getCallbackUri')
const { internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const clientId = process.env.CLIENT_ID
    const clientSecret = process.env.CLIENT_SECRET
    const poolId = process.env.COGNITO_POOL_ID
    const codeVerifier = process.env.CODE_VERIFIER
    const headers = getHeaders(event, true)
    const host = get(headers, 'host', process.env.DOMAIN)
    const protocol = host.startsWith('localhost') ? 'http' : 'https'
    const redirectUri = getCallbackUri(`${protocol}://${host}`)
    const providerWellKnown = `https://cognito-idp.eu-west-1.amazonaws.com/${poolId}/.well-known/openid-configuration`

    const cognitoIssuer = await Issuer.discover(providerWellKnown)

    const client = new cognitoIssuer.Client({
      client_id: clientId,
      client_secret: clientSecret,
      redirect_uris: [redirectUri],
      response_types: ['code']
    })

    const queryString = qs.stringify(event.queryStringParameters)
    const callbackParams = {
      url: `${event.path}?${queryString}`,
      method: event.httpMethod
    }

    log.debug({ callbackParams }, 'auth callback params')

    const params = client.callbackParams(callbackParams)
    // todo - use a proper nonce at some point
    const tokenSet = await client.callback(redirectUri, params, { code_verifier: codeVerifier, nonce: 'foobar' })

    log.info({ claims: tokenSet.claims(), tokenSet }, 'token claims returned')

    const returnPath = getCookie(event, 'LoginReturnPath')

    return {
      statusCode: 302,
      headers: {
        Location: returnPath || '/',
        'Set-Cookie': setCookie('AuthToken', tokenSet.id_token)
      },
      body: null
    }
  } catch (err) {
    log.error({ err }, 'auth callback error')
    return internalError
  }
}
