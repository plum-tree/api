const { deleteCookie } = require('../../helpers/cookies')

exports.handler = async () => {
  return {
    statusCode: 302,
    headers: {
      Location: '/',
      'Strict-Transport-Security': 'max-age=31536000; includeSubDomains; preload',
      'Set-Cookie': deleteCookie('AuthToken')
    },
    body: null
  }
}
