const get = require('lodash.get')
const { URL } = require('url')
const oidcParams = require('../../helpers/oidcParams')
const log = require('../../helpers/log')
const { setCookie } = require('../../helpers/cookies')

/**
 * Call used for signup and login. Depending on the args (query params) passed
 * will either:
 * - launch the signup hosted UI
 * - launch the login hosted UI
 * This is all stored in one Lambda so we can use provisioned concurrency on
 * only a single Lambda rather than multiple for each action.
 */
exports.handler = async (event) => {
  try {
    const action = get(event, 'queryStringParameters.action', 'login')
    const returnUrl = get(event, 'queryStringParameters.returnUrl', `https://${process.env.DOMAIN}`)

    log.info(
      { action, returnUrl },
      'Building OIDC params')

    // for login and signup determine where to redirect once authentication is
    // done.
    const url = new URL(returnUrl)
    const returnPath = url.pathname
    const returnDomain = url.origin

    const queryParams = await oidcParams(returnDomain)

    log.info(
      { queryParams, action },
      `Redirecting ${action} request with OIDC params`)

    const validActions = ['signup', 'login']
    if (validActions.includes(action)) {
      return {
        statusCode: 302,
        headers: {
          'Strict-Transport-Security': 'max-age=31536000; includeSubDomains; preload',
          'Set-Cookie': setCookie('LoginReturnPath', returnPath),
          Location: `https://auth.${process.env.DOMAIN}/${action}?${queryParams}`
        },
        body: null
      }
    } else {
      return {
        statusCode: 400,
        headers: {
          'Strict-Transport-Security': 'max-age=31536000; includeSubDomains; preload',
          'X-Content-Type-Options': 'nosniff',
          'Content-Type': 'text/plain'
        },
        body: 'invalid auth action'
      }
    }
  } catch (err) {
    log.error(
      { err },
      'auth launch error')

    return {
      statusCode: 500,
      headers: {
        'Strict-Transport-Security': 'max-age=31536000; includeSubDomains; preload',
        'X-Content-Type-Options': 'nosniff',
        'Content-Type': 'text/plain'
      },
      body: 'error'
    }
  }
}
