const log = require('../../helpers/log')
const getSession = require('../../helpers/getSession')
const { getTrees } = require('../../helpers/dynamodb')
const { unauthorized, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const trees = await getTrees(sub)

    return {
      statusCode: 200,
      body: JSON.stringify(trees)
    }
  } catch (err) {
    log.error({ err }, 'Failed to get users trees')
    return internalError
  }
}
