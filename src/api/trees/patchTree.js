const get = require('lodash.get')
const log = require('../../helpers/log')
const getSession = require('../../helpers/getSession')
const getRequestBody = require('../../helpers/getRequestBody')
const { getTree, updateTree, isImageUsedInPublishedTree } = require('../../helpers/dynamodb')
const { deleteProcessedImage, moveFromInputToProcessed } = require('../../helpers/images')
const { unauthorized, notFound, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const treeId = get(event, 'pathParameters.id')
    const tree = getRequestBody(event)
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const origTree = await getTree(sub, treeId)
    if (!origTree) {
      return notFound('The tree does not exist.')
    }

    // title cant be empty/blank if set
    if ('title' in tree && tree.title.trim() === '') {
      const errors = [{
        title: 'Invalid Attribute',
        detail: 'The field "title" is required.'
      }]

      log.warn({ errors }, 'Tree update had validation errors')
      return {
        statusCode: 400,
        body: JSON.stringify({ errors })
      }
    }

    const updatedTree = await updateTree(sub, treeId, tree)

    if (tree.cover !== undefined) {
      const origTreeCover = get(origTree, 'cover', null)
      const newTreeCover = get(tree, 'cover', null)

      if (newTreeCover && newTreeCover !== origTreeCover) {
        await moveFromInputToProcessed(newTreeCover)
      }

      if (origTreeCover && newTreeCover !== origTreeCover) {
        const publishedImage = await isImageUsedInPublishedTree(treeId, origTreeCover)
        if (!publishedImage) {
          await deleteProcessedImage(origTreeCover)
        }
      }
    }

    return {
      statusCode: 200,
      body: JSON.stringify(updatedTree)
    }
  } catch (err) {
    log.error({ err }, 'Failed to update tree')
    return internalError
  }
}
