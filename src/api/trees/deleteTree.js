const get = require('lodash.get')
const log = require('../../helpers/log')
const getSession = require('../../helpers/getSession')
const { deleteTree } = require('../../helpers/dynamodb')
const { unauthorized, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const treeId = get(event, 'pathParameters.id')
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    await deleteTree(sub, treeId)
    // todo - unpublish tree if published
    // await unpublishTree(treeId, sub)

    return {
      statusCode: 204,
      body: null
    }
  } catch (err) {
    log.error({ err }, 'Failed to delete tree')
    return internalError
  }
}
