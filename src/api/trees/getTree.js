const get = require('lodash.get')
const log = require('../../helpers/log')
const getSession = require('../../helpers/getSession')
const { getTree } = require('../../helpers/dynamodb')
const { unauthorized, notFound, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const treeId = get(event, 'pathParameters.id')
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const tree = await getTree(sub, treeId)

    if (!tree) {
      return notFound('The tree does not exist.')
    }

    return {
      statusCode: 200,
      body: JSON.stringify(tree)
    }
  } catch (err) {
    log.error({ err }, 'Failed to get tree')
    return internalError
  }
}
