const get = require('lodash.get')
const getSession = require('../../helpers/getSession')
const log = require('../../helpers/log')
const { getPeople } = require('../../helpers/dynamodb')
const { unauthorized, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const treeId = get(event, 'queryStringParameters.tree')
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const people = await getPeople(sub, treeId)

    return {
      statusCode: 200,
      body: JSON.stringify(people)
    }
  } catch (err) {
    log.error({ err }, 'Failed to get person')
    return internalError
  }
}
