const get = require('lodash.get')
const getSession = require('../../helpers/getSession')
const log = require('../../helpers/log')
const getRequestBody = require('../../helpers/getRequestBody')
const { getPerson, getTrees, updatePerson, isImageUsedInPublishedTree } = require('../../helpers/dynamodb')
const { moveFromInputToProcessed, deleteProcessedImage } = require('../../helpers/images')
const { unauthorized, notFound, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const personId = get(event, 'pathParameters.id')
    const person = getRequestBody(event)
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const dbRecord = await getPerson(sub, personId)

    if (!dbRecord) {
      return notFound('The person does not exist.')
    }

    // validate the trees the person is assigned to are all owned by the current
    // user
    const userTrees = await getTrees(sub)
    const userTreeIds = userTrees.map(t => t.id)
    const { trees: personTreeIds } = person

    for (let i = 0; i < personTreeIds; i++) {
      const treeId = personTreeIds[i]
      if (!userTreeIds.includes(treeId)) {
        const errors = [{
          title: 'Invalid Attribute',
          detail: 'The tree does not exist'
        }]

        log.warn({ errors, sub, userTreeIds, personTreeIds }, 'Person create failed as a tree was not found')
        return {
          statusCode: 400,
          body: JSON.stringify({ errors })
        }
      }
    }

    const updatedPerson = await updatePerson(sub, personId, person)

    const origAvatar = get(dbRecord, 'avatar', null)
    const newAvatar = get(person, 'avatar', null)

    if (newAvatar && newAvatar !== origAvatar) {
      await moveFromInputToProcessed(newAvatar)
    }

    if (origAvatar && newAvatar !== origAvatar) {
      for (let i = 0; i < personTreeIds; i++) {
        const treeId = personTreeIds[i]
        const publishedImage = await isImageUsedInPublishedTree(treeId, origAvatar)
        if (!publishedImage) {
          await deleteProcessedImage(origAvatar)
        }
      }
    }

    return {
      statusCode: 200,
      body: JSON.stringify(updatedPerson)
    }
  } catch (err) {
    log.error({ err }, 'Failed to get person')
    return internalError
  }
}
