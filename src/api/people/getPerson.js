const get = require('lodash.get')
const getSession = require('../../helpers/getSession')
const log = require('../../helpers/log')
const { getPerson } = require('../../helpers/dynamodb')
const { unauthorized, notFound, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const personId = get(event, 'pathParameters.id')
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const person = await getPerson(sub, personId)

    if (!person) {
      return notFound('The person does not exist.')
    }

    return {
      statusCode: 200,
      body: JSON.stringify(person)
    }
  } catch (err) {
    log.error({ err }, 'Failed to get person')
    return internalError
  }
}
