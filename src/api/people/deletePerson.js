const get = require('lodash.get')
const getSession = require('../../helpers/getSession')
const log = require('../../helpers/log')
const { deletePerson } = require('../../helpers/dynamodb')
const { unauthorized, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const personId = get(event, 'pathParameters.id')
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    await deletePerson(sub, personId)

    return {
      statusCode: 204,
      body: null
    }
  } catch (err) {
    log.error({ err }, 'Failed to delete person')
    return internalError
  }
}
