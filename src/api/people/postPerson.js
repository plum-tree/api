const getSession = require('../../helpers/getSession')
const getRequestBody = require('../../helpers/getRequestBody')
const { getTrees, createPerson } = require('../../helpers/dynamodb')
const { moveFromInputToProcessed } = require('../../helpers/images')
const log = require('../../helpers/log')
const { unauthorized, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const person = getRequestBody(event)
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session

    // validate the trees the person is assigned to are all owned by the current
    // user
    const userTrees = await getTrees(sub)
    const userTreeIds = userTrees.map(t => t.id)
    const { trees: personTreeIds } = person

    for (let i = 0; i < personTreeIds; i++) {
      const treeId = personTreeIds[i]
      if (!userTreeIds.includes(treeId)) {
        const errors = [{
          title: 'Invalid Attribute',
          detail: 'The tree does not exist'
        }]

        log.warn({ errors, sub, userTreeIds, personTreeIds }, 'Person create failed as a tree was not found')
        return {
          statusCode: 400,
          body: JSON.stringify({ errors })
        }
      }
    }

    const createdPerson = await createPerson(sub, person)

    if (createdPerson.avatar) {
      await moveFromInputToProcessed(createdPerson.avatar)
    }

    return {
      statusCode: 200,
      body: JSON.stringify(createdPerson)
    }
  } catch (err) {
    log.error({ err }, 'Failed to create person')
    return internalError
  }
}
