const sharp = require('sharp')
const { S3Client, GetObjectCommand, PutObjectCommand, HeadObjectCommand } = require('@aws-sdk/client-s3')
const log = require('../../helpers/log')
const { validationError, notFound, tooLarge, internalError } = require('../../helpers/responses')

const client = new S3Client({ region: 'eu-west-1' })
const Bucket = process.env.UPLOAD_BUCKET_PROCESSED

/**
 * Performs the steps to get a resized/scaled down version of an image. We get
 * the width, height and image all from the API request path. We then do the
 * following steps:
 * 1. validate the width and height are valid.
 *    (e.g /api/upload/fooxbar/dog.jpeg is invalid as foo and bar are not valid
 *    numbers).
 * 2. validate the image exists in the processed bucket.
 * 3. check if we already have a resized version in the processed bucket (if yes
 *    return that).
 * 4. if no resized image already exists generate one from the original.
 * 5. save it resized image to s3 for future use.
 * 6. return the newly resized image.
 * @param {Object} event
 * @returns
 */
exports.handler = async (event) => {
  try {
    log.info('Get resized image')

    // validate the width and height are valid
    const { dimensions, image } = event.pathParameters
    const { width, height, error: dimensionsError } = extractDimensions(dimensions)
    if (dimensionsError) {
      log.warn({ dimensions }, 'invalid dimensions given')
      return validationError(validationError)
    }

    // validate the image exists in the processed bucket
    const imageFound = await imageExists(image)
    if (!imageFound) {
      log.info({ image }, 'image not found')
      return notFound('Image not found.')
    }

    // check if we already have a resized version in the processed bucket
    const resizedName = getResizedImageName(image, width, height)
    const resizedImageFound = await imageExists(resizedName)
    if (resizedImageFound) {
      log.info({ image }, 'resized image found so using that')
      const { Body, ContentType } = await getImageFromS3(resizedName)

      return {
        statusCode: 200,
        body: await Body.transformToString('base64'),
        isBase64Encoded: true,
        headers: {
          'Content-Type': ContentType || 'image'
        }
      }
    }
    log.info({ image }, 'resized image not found')

    // no resized image exists so generate one from the original
    const { Body, ContentType } = await getImageFromS3(image)
    const newImage = await sharp(await Body.transformToByteArray())
      .rotate()
      .resize({ width, height })
      .toBuffer()

    // save it resized image to s3 for future use
    await saveImageToS3(resizedName, newImage)

    // return the newly resized image
    const body = newImage.toString('base64')
    const lambdaPayloadLimit = 6 * 1024 * 1024
    if (body.length > lambdaPayloadLimit) {
      log.warn({ image, size: body.length }, 'image too large')
      return tooLarge('The converted image is too large to return.')
    }

    return {
      statusCode: 200,
      body,
      isBase64Encoded: true,
      headers: {
        'Content-Type': ContentType || 'image'
      }
    }
  } catch (err) {
    log.error({ err }, 'Get resized image error')
    return internalError
  }
}

const extractDimensions = dimensions => {
  // validate format "NumberxNumber"
  const regex = /^\d+x\d+$/
  if (regex.test(dimensions)) {
    const [width, height] = dimensions.split('x').map(i => parseInt(i))
    return { width, height }
  }

  return {
    error: 'Invalid dimensions'
  }
}

const imageExists = async (Key) => {
  try {
    // early escape (only migrated cover or avatar images can be resized)
    if (!Key.startsWith('cover/') && !Key.startsWith('avatar/')) {
      log.info('image is not a cover/avatar image')
      return false
    }

    const command = new HeadObjectCommand({ Bucket, Key })
    await client.send(command)

    return true
  } catch (err) {
    if (err.name === 'NotFound') {
      log.info({ Key }, 'image was not found during image exists check')
      return false
    }
    log.error({ err }, 'failed to check if image exists')
    throw err
  }
}

const getResizedImageName = (image, width, height) => {
  const parts = image.split('.')
  return `${parts[0]}-${width}x${height}.${parts[1]}`
}

const getImageFromS3 = (Key) => {
  log.info(`Getting ${Key} from bucket`)
  const command = new GetObjectCommand({ Bucket, Key })
  return client.send(command)
}

const saveImageToS3 = (Key, Body) => {
  log.info(`saving ${Key} to uploads bucket`)

  const ContentType = getContentType(Key)
  const params = {
    Bucket,
    Key,
    Body,
    ContentType
  }

  const command = new PutObjectCommand(params)
  return client.send(command)
}

const getContentType = image => {
  return image.split('.')[1] === 'png' ? 'image/png' : 'image/jpeg'
}
