const sharp = require('sharp')
const uuid = require('uuid')
const { S3Client, PutObjectCommand, GetObjectCommand } = require('@aws-sdk/client-s3')
const getSession = require('../../helpers/getSession')
const getRequestBody = require('../../helpers/getRequestBody')
const log = require('../../helpers/log')
const { unauthorized, internalError } = require('../../helpers/responses')

const client = new S3Client({ region: 'eu-west-1' })

const getNewFilename = (image) => {
  const actionId = uuid.v4()
  const ext = image.split('.').pop()

  if (image.startsWith('avatar/')) {
    return `avatar/${actionId}.${ext}`
  } else if (image.startsWith('cover/')) {
    return `cover/${actionId}.${ext}`
  }
  return `cover/${actionId}.${ext}`
}

const crop = (imageName, imageBuffer, imageContentType, extract) => {
  return new Promise((resolve, reject) => {
    sharp(imageBuffer)
      .rotate()
      .extract(extract)
      .toBuffer(async (err, Body) => {
        if (err) {
          log.error({ err, extract, imageName }, 'Failed to crop image')
          return reject(err)
        }

        const Key = getNewFilename(imageName)
        const Bucket = process.env.UPLOAD_BUCKET_INPUT
        const ContentType = imageContentType
        const params = {
          Bucket,
          Key,
          ContentType,
          Body
        }

        const command = new PutObjectCommand(params)
        return client.send(command)
          .then(() => resolve(Key))
      })
  })
}

exports.handler = async (event) => {
  try {
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const { cropData, image } = getRequestBody(event)
    const { x, y, width, height } = cropData
    const extract = { left: x, top: y, width, height }

    log.info({ extract, image, sub }, 'I will now crop an image.')

    const Key = image
    const Bucket = process.env.UPLOAD_BUCKET_INPUT
    const command = new GetObjectCommand({ Bucket, Key })
    const fetchedFileDataFromAWS = await client.send(command)

    const { Body, ContentType } = fetchedFileDataFromAWS
    const imageBuffer = Buffer.from(await Body.transformToByteArray())

    const newFilename = await crop(image, imageBuffer, ContentType, extract)

    return {
      statusCode: 200,
      isBase64Encoded: false,
      body: JSON.stringify({
        filename: newFilename
      })
    }
  } catch (err) {
    log.error({ err }, 'Failed to crop image')
    return internalError
  }
}
