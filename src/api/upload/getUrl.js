const uuid = require('uuid')
const get = require('lodash.get')
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner')
const { S3Client, PutObjectCommand } = require('@aws-sdk/client-s3')
const log = require('../../helpers/log')
const getSession = require('../../helpers/getSession')
const { unauthorized, validationError, internalError } = require('../../helpers/responses')

exports.handler = async event => {
  try {
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const actionId = uuid.v4()
    const type = get(event, 'queryStringParameters.type')
    const dir = get(event, 'queryStringParameters.dir')
    const acceptedFileTypes = ['image/png', 'image/jpeg']
    const acceptedDirs = ['avatar', 'cover']

    if (!acceptedFileTypes.includes(type)) {
      log.info({ sub }, `invalid file type "${type}" for upload`)
      return validationError('Invalid file type only png or jpeg allowed.')
    }

    if (!acceptedDirs.includes(dir)) {
      log.info({ sub }, `invalid file directory "${dir}" for upload`)
      return validationError('Invalid upload location.')
    }

    const ext = type === 'image/png' ? 'png' : 'jpg'

    const client = new S3Client({ region: 'eu-west-1' })
    const command = new PutObjectCommand({
      Bucket: process.env.UPLOAD_BUCKET_INPUT,
      Key: `${dir}/${actionId}.${ext}`,
      ContentType: type
    })
    const uploadURL = await getSignedUrl(client, command, { expiresIn: 3600 })

    return {
      statusCode: 200,
      isBase64Encoded: false,
      body: JSON.stringify({
        uploadURL,
        filename: `${dir}/${actionId}.${ext}`
      })
    }
  } catch (err) {
    log.error({ err })
    return internalError
  }
}
