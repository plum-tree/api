const get = require('lodash.get')
const log = require('../../helpers/log')
const getSession = require('../../helpers/getSession')
const { getPublishedTree, publishTree } = require('../../helpers/dynamodb')
const { deleteOrphanedTreeCover, deleteOrphanedPeopleAvatars } = require('../../helpers/images')
const { notFound, unauthorized, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const treeId = get(event, 'pathParameters.id')
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const origPublishedTree = await getPublishedTree(treeId)
    const publishedTree = await publishTree(treeId, sub)

    if (!publishedTree) {
      return notFound('The tree does not exist.')
    }

    // Remove old/orphaned cover image
    await deleteOrphanedTreeCover(origPublishedTree, publishedTree)

    // Remove old/orphaned people avatars
    const origPeople = get(origPublishedTree, 'people', [])
    const newPeople = get(publishedTree, 'people', [])
    await deleteOrphanedPeopleAvatars(origPeople, newPeople)

    return {
      statusCode: 204,
      body: null
    }
  } catch (err) {
    log.error({ err }, 'Failed to publish tree')
    return internalError
  }
}
