const get = require('lodash.get')
const log = require('../../helpers/log')
const getSession = require('../../helpers/getSession')
const { getTree, unpublishTree, getPeople } = require('../../helpers/dynamodb')
const { deleteOrphanedTreeCover, deleteOrphanedPeopleAvatars } = require('../../helpers/images')
const { notFound, unauthorized, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const treeId = get(event, 'pathParameters.id')
    const session = getSession(event)

    if (!session) {
      return unauthorized
    }

    const { sub } = session
    const tree = await getTree(sub, treeId)

    if (!tree) {
      return notFound('The tree does not exist.')
    }

    const unpublishedTree = await unpublishTree(treeId, sub)
    const people = await getPeople(sub, treeId)

    // Remove old/orphaned cover image
    await deleteOrphanedTreeCover(unpublishedTree, tree)

    // Remove old/orphaned people avatars
    const origPeople = get(unpublishedTree, 'people', [])
    await deleteOrphanedPeopleAvatars(origPeople, people)

    return {
      statusCode: 204,
      body: null
    }
  } catch (err) {
    log.error({ err }, 'Failed to unpublish tree')
    return internalError
  }
}
