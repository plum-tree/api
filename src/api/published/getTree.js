const get = require('lodash.get')
const log = require('../../helpers/log')
const { getPublishedTree } = require('../../helpers/dynamodb')
const { notFound, internalError } = require('../../helpers/responses')

exports.handler = async (event) => {
  try {
    const treeId = get(event, 'pathParameters.id')
    const tree = await getPublishedTree(treeId)

    if (!tree) {
      return notFound('The published tree does not exist.')
    }

    return {
      statusCode: 200,
      body: JSON.stringify(tree)
    }
  } catch (err) {
    log.error({ err }, 'Failed to get published tree')
    return internalError
  }
}
