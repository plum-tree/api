/**
 * Gets the redirect URI based on the host (header) passed so we can login and
 * be correctly redirected to the correct color sub domain. Because the host
 * header is not 100% reliable falls back to process.env.DOMAIN as host.
 */
module.exports = (origin) => {
  const validOrigins = [
    `https://blue.${process.env.DOMAIN}`,
    `https://green.${process.env.DOMAIN}`,
    'http://localhost:8080' // local dev env
  ]

  if (validOrigins.includes(origin)) {
    return `${origin}/api/auth/callback`
  }
  return `https://${process.env.DOMAIN}/api/auth/callback`
}
