const {
  DynamoDBClient,
  QueryCommand,
  PutItemCommand,
  GetItemCommand,
  UpdateItemCommand,
  DeleteItemCommand
} = require('@aws-sdk/client-dynamodb')
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb')
const client = new DynamoDBClient({ region: 'eu-west-1' })
const uuid = require('uuid')
const get = require('lodash.get')

// General DynamoDB notes:
// 1. DynamoDB name conventions tend to be CamelCase for attributes/columns
//    however these util functions will update these to lowerCamelCase. Either
//    from the inputs it expects to the outputs it returns. This is to match
//    what we want our API JSON attribute names to follow.
// 2. These methods do not validate ownership of records and expects that to
//    have already been done before calling. For example creating a person and
//    passing the tree Ids that person is to be created for does not check the
//    current user owns said tree(s). Though many of these methods require a
//    user Id as the partition key so updating a tree with an invalid/incorrect
//    user would simply not find the record to update.
// 3. Function args will (where applicable) start with the userId, then objectId
//    and finally the object in question. The exception being published tree
//    functions that don't use the userId as a the partition key. An example
//    being updateTree where the first arg is the userId, second is the treeId
//    and last is new tree object to update to.

/**
 * Given an object generates the Dynamo update expression attributes so we can
 * add the to UpdateItemCommand and only update attributes we have data for.
 * Example:
 * passing the object `{title: "foo", description: "bar"} the function returns
 *  {
 *    UpdateExpression: "set #title = :title, #description = :description",
 *    ExpressionAttributeNames: {
 *      "#title": "Title",
 *      "#description": "Description",
 *    },
 *    ExpressionAttributeValues: {
 *      ":title": { S: "foo" },
 *      ":description": { S: "bar" },
 *    }
 *  }
 * Note how it converts our lowerCamelCase to CamelCase for the dynamo attribute
 * names.
 * @param {Object} object
 * @returns {Object}
 */
const generateUpdateExpression = (object) => {
  const update = {
    UpdateExpression: 'set',
    ExpressionAttributeNames: {},
    ExpressionAttributeValues: {}
  }
  for (const [attribute, value] of Object.entries(object)) {
    const titleAttr = attribute.charAt(0).toUpperCase() + attribute.slice(1)
    update.UpdateExpression += ` #${attribute} = :${attribute},`
    update.ExpressionAttributeNames[`#${attribute}`] = titleAttr
    update.ExpressionAttributeValues[`:${attribute}`] = marshall(value, {
      convertTopLevelContainer: true
    })
  }
  update.UpdateExpression = update.UpdateExpression.slice(0, -1)

  return update
}

/**
 * Gets all the trees for a specific user querying Dynamo using the partition
 * key only. We only return a list of tree ids and titles since thats all we
 * need in the plum tree side nav where this is called.
 * @param {String} userId
 * @returns {Promise(Array)}
 */
const getTrees = (userId) => {
  const input = {
    TableName: 'Trees',
    ExpressionAttributeValues: {
      ':userId': {
        S: userId
      }
    },
    KeyConditionExpression: 'UserId = :userId',
    ProjectionExpression: 'TreeId,Title'
  }
  const command = new QueryCommand(input)
  return client.send(command)
    .then(response => response.Items)
    .then(trees => trees.map(tree => {
      const { TreeId: id, Title: title } = unmarshall(tree)
      return { id, title }
    }))
}

/**
 * Query a single tree for a user using the treeId and userId (both partition
 * key and sort key).
 * @param {String} userId
 * @param {String} treeId
 * @returns {Promise(Object)}
 */
const getTree = async (userId, treeId) => {
  const input = {
    TableName: 'Trees',
    Key: {
      UserId: {
        S: userId
      },
      TreeId: {
        S: treeId
      }
    }
  }
  const command = new GetItemCommand(input)
  const { Item } = await client.send(command)

  if (!Item || Item.Deleted) {
    return null
  }

  const {
    TreeId: id,
    Title: title,
    Cover: cover,
    Description: description,
    Data: data
  } = unmarshall(Item)

  return { id, title, cover, description, data }
}

/**
 * Creates a new tree record for a user. This tree is created with a new uuid as
 * its Id and default "data" for an empty tree. The newly created tree is
 * returned on creation.
 * @param {String} userId
 * @param {Object} tree
 * @returns {Promise(Object)}
 */
const createTree = (userId, { title, description, cover }) => {
  const tree = {
    UserId: userId,
    TreeId: uuid.v4(),
    Title: title,
    Description: description,
    Cover: cover,
    Data: {
      person: null,
      partners: []
    }
  }
  const input = {
    TableName: 'Trees',
    Item: marshall(tree)
  }
  const command = new PutItemCommand(input)
  return client.send(command)
    .then(() => ({
      id: tree.TreeId,
      title,
      description,
      cover,
      data: tree.Data
    }))
}

/**
 * Given a tree object updates the DynamoDB record for that tree with the new
 * attribute vales in the given object. Attributes not passed are not updated or
 * removed. This means we can pass a subset of attributes to update (e.g. only
 * provide a new title) and not worry about other attributes being deleted.
 * @param {String} userId
 * @param {String} treeId
 * @param {String} tree
 * @returns {Promise(Object)}
 */
const updateTree = (userId, treeId, tree) => {
  const input = {
    TableName: 'Trees',
    Key: {
      UserId: {
        S: userId
      },
      TreeId: {
        S: treeId
      }
    },
    ...generateUpdateExpression(tree)
  }
  const command = new UpdateItemCommand(input)
  return client.send(command)
}

/**
 * Updates a tree setting the deleted attribute to the current date/time. We set
 * this to a date so we can come back later to cleanup trees deleted a while ago
 * when we're more certain we don't need to revert the soft delete.
 * @param {String} userId
 * @param {String} treeId
 * @returns {Promise}
 */
const deleteTree = (userId, treeId) => {
  const input = {
    TableName: 'Trees',
    Key: {
      UserId: {
        S: userId
      },
      TreeId: {
        S: treeId
      }
    },
    ...generateUpdateExpression({ deleted: new Date().toISOString() })
  }
  const command = new UpdateItemCommand(input)
  return client.send(command)
}

/**
 * Gets the people for a given tree.
 * @param {String} userId
 * @param {String} treeId
 * @returns {Promise(Array)}
 */
const getPeople = (userId, treeId) => {
  const input = {
    TableName: 'People',
    ExpressionAttributeValues: {
      ':userId': {
        S: userId
      },
      ':treeId': {
        S: treeId
      }
    },
    KeyConditionExpression: 'UserId = :userId',
    ProjectionExpression: 'PersonId,Avatar,FirstName,LastName',
    FilterExpression: 'contains(#treeIds, :treeId)',
    ExpressionAttributeNames: { '#treeIds': 'TreeIds' }
  }
  const command = new QueryCommand(input)
  return client.send(command)
    .then(response => response.Items)
    .then(people => people.map(person => {
      const {
        PersonId: id,
        Avatar: avatar,
        FirstName: firstName,
        LastName: lastName
      } = unmarshall(person)
      return { id, avatar, firstName, lastName }
    }))
}

/**
 * Gets a single person record given the users Id and the person Id.
 * @param {String} userId
 * @param {String} personId
 * @returns {Promise(Object)}
 */
const getPerson = (userId, personId) => {
  const input = {
    TableName: 'People',
    Key: {
      UserId: {
        S: userId
      },
      PersonId: {
        S: personId
      }
    }
  }
  const command = new GetItemCommand(input)
  return client.send(command)
    .then(response => response.Item)
    .then(unmarshall)
    .then(({
      PersonId: id,
      TreeIds: trees,
      Avatar: avatar,
      FirstName: firstName,
      LastName: lastName,
      Bio: bio,
      Traits: traits,
      Aspirations: aspirations,
      LifeStates: lifeStates,
      Custom: custom
    }) => {
      return {
        id,
        trees,
        avatar,
        firstName,
        lastName,
        bio,
        traits,
        aspirations,
        lifeStates,
        custom
      }
    })
}

/**
 * Creates a new person to be placed in tree(s). Returns the newly created
 * person record.
 * @param {String} userId
 * @param {Object} person
 * @returns {Promise(Object)}
 */
const createPerson = (userId, person) => {
  const personId = uuid.v4()
  const {
    trees,
    avatar,
    firstName,
    lastName,
    bio,
    traits,
    aspirations,
    lifeStates,
    custom
  } = person
  const item = {
    UserId: userId,
    PersonId: personId,
    TreeIds: trees,
    Avatar: avatar,
    FirstName: firstName,
    LastName: lastName,
    Bio: bio,
    Traits: traits,
    Aspirations: aspirations,
    LifeStates: lifeStates,
    Custom: custom
  }
  const input = {
    TableName: 'People',
    Item: marshall(item)
  }
  const command = new PutItemCommand(input)
  return client.send(command)
    .then(() => ({
      id: personId,
      trees,
      avatar,
      firstName,
      lastName,
      bio,
      traits,
      aspirations,
      lifeStates,
      custom
    }))
}

/**
 * Updates a person record and returns the updated person record.
 * @param {String} userId
 * @param {String} personId
 * @param {Object} person
 * @returns {Promise(Object)}
 */
const updatePerson = (userId, personId, person) => {
  const input = {
    TableName: 'People',
    Key: {
      UserId: {
        S: userId
      },
      PersonId: {
        S: personId
      }
    },
    ...generateUpdateExpression(person)
  }
  const command = new UpdateItemCommand(input)
  return client.send(command)
}

/**
 * Updates a person setting the deleted attribute to the current date/time. We
 * set this to a date so we can come back later to cleanup people deleted a
 * while ago when we're more certain we don't need to revert the soft delete.
 * @param {String} userId
 * @param {String} personId
 * @param {Object} person
 * @returns {Promise(Object)}
 */
const deletePerson = (userId, personId) => {
  const input = {
    TableName: 'People',
    Key: {
      UserId: {
        S: userId
      },
      PersonId: {
        S: personId
      }
    },
    ...generateUpdateExpression({ deleted: new Date().toISOString() })
  }
  const command = new UpdateItemCommand(input)
  return client.send(command)
}

/**
 * Tries to find a published tree. Published tree records have the same Ids as
 * the tree they are derived from. However unlike getting a tree from the Trees
 * table we don't always know the user Id (e.g. user is not logged in or viewing
 * a published tree from another user) so we query on TreeId only.
 *
 * A published tree records includes the tree data and people list for that tree
 * plus a the date the tree was published. New publishes overwrite the record
 * with a the latest tree and people. This means a tree and it's published
 * version are not always in sync and a published tree is just a snapshot of the
 * tree at publish time.
 * @param {String} treeId
 * @returns {Promise(Object)}
 */
const getPublishedTree = async (treeId) => {
  const input = {
    TableName: 'Published',
    ExpressionAttributeValues: {
      ':treeId': {
        S: treeId
      }
    },
    KeyConditionExpression: 'TreeId = :treeId'
  }
  const command = new QueryCommand(input)
  const { Items } = await client.send(command)
  const Item = Items[0]

  if (!Item || Item.Deleted) {
    return null
  }

  const {
    TreeId: id,
    Title: title,
    Cover: cover,
    Description: description,
    Data: data,
    People: people,
    LastPublishDate: lastPublishDate
  } = unmarshall(Item)

  return { id, title, cover, description, data, people, lastPublishDate }
}

/**
 * Publishes a tree (saves a copy of the tree and its people to the Published
 * table). Returns the created record which also includes a lastPublishDate.
 * @param {String} treeId
 * @param {String} userId
 * @returns {Promise(Object)}
 */
const publishTree = async (treeId, userId) => {
  const { title, description, cover, data } = await getTree(userId, treeId)
  const people = await getPeople(userId, treeId)
  const lastPublishDate = new Date().toISOString()

  const published = {
    TreeId: treeId,
    UserId: userId,
    Title: title,
    Description: description,
    Cover: cover,
    Data: data,
    People: people,
    LastPublishDate: lastPublishDate
  }

  const input = {
    TableName: 'Published',
    Item: marshall(published)
  }
  const command = new PutItemCommand(input)
  return client.send(command)
    .then(() => ({
      id: treeId,
      title,
      description,
      cover,
      data,
      people,
      lastPublishDate
    }))
}

/**
 * Adds a deleted flag to a published tree record. These don't show up as public
 * until re-published.
 * @param {String} treeId
 * @param {String} userId
 * @returns
 */
const unpublishTree = async (treeId, userId) => {
  const input = {
    TableName: 'Published',
    Key: {
      TreeId: {
        S: treeId
      },
      UserId: {
        S: userId
      }
    },
    ReturnValues: 'ALL_OLD'
  }
  const command = new DeleteItemCommand(input)
  const { Attributes } = await client.send(command)

  const {
    Title: title,
    Description: description,
    Cover: cover,
    Data: data,
    People: people,
    LastPublishDate: lastPublishDate,
    Deleted: deleted
  } = unmarshall(Attributes)

  return {
    id: treeId,
    userId,
    title,
    description,
    cover,
    data,
    people,
    lastPublishDate,
    deleted
  }
}

/**
 * Checks if image is used as tree cover or persons avatar in published tree.
 * @param {String} treeId
 * @param {String} image
 * @returns
 */
const isImageUsedInPublishedTree = async (treeId, image) => {
  if (!image) {
    return false
  }

  const publishedTree = await getPublishedTree(treeId)

  // check cover
  const cover = get(publishedTree, 'cover')
  if (image === cover) {
    return true
  }

  // check avatars
  const avatars = get(publishedTree, 'people', []).map(p => p.avatar)
  if (avatars.includes(image)) {
    return true
  }

  return false
}

module.exports = {
  getTrees,
  getTree,
  createTree,
  updateTree,
  deleteTree,
  createPerson,
  getPerson,
  updatePerson,
  getPeople,
  deletePerson,
  getPublishedTree,
  publishTree,
  unpublishTree,
  isImageUsedInPublishedTree
}
