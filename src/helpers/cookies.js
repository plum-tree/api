const get = require('lodash.get')
const cookie = require('cookie')
const getHeaders = require('./getHeaders')

/**
 * Utility to extract a cookie value from an event object.
 */
const getCookie = (event, cookieName) => {
  const headers = getHeaders(event, false)
  const cookies = cookie.parse(get(headers, 'cookie', ''))
  return cookies[cookieName]
}

/**
 * Utility to build the cookie header to save a cookie.
 * Note: this does not actually set the cookie, you need to use the returned
 * value in a response header
 */
const setCookie = (name, value, age = 3600) => {
  const env = process.env.NODE_ENV
  return `${name}="${value}"; Path=/; Max-Age=${age}${env === 'production' ? `; Secure; domain=${process.env.DOMAIN}; httpOnly` : ''}`
}

const deleteCookie = (name) => {
  const env = process.env.NODE_ENV
  return `${name}="null"; Path=/; Expires=Thur, 01 Jan 1970 00:00:00 GMT${env === 'production' ? `; Secure; domain=${process.env.DOMAIN}; httpOnly` : ''}`
}

module.exports = {
  getCookie,
  setCookie,
  deleteCookie
}
