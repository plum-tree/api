const unauthorized = {
  statusCode: 403,
  body: JSON.stringify({
    errors: [{
      title: 'Unauthorized',
      detail: 'Invalid user session, try logging in again.'
    }]
  })
}

const internalError = {
  statusCode: 500,
  body: JSON.stringify({
    errors: [{
      title: 'Internal Error',
      detail: 'Something went wrong, please try again later.'
    }]
  })
}

const notFound = (message) => ({
  statusCode: 404,
  body: JSON.stringify({
    errors: [{
      title: 'Not Found',
      detail: message || 'Not found.'
    }]
  })
})

const validationError = (message) => ({
  statusCode: 404,
  body: JSON.stringify({
    errors: [{
      title: 'Validation Error',
      detail: message || 'Invalid request.'
    }]
  })
})

const tooLarge = (message) => ({
  statusCode: 413,
  body: JSON.stringify({
    errors: [{
      title: 'Too Large',
      detail: message || 'Too large'
    }]
  })
})

module.exports = {
  unauthorized,
  internalError,
  notFound,
  validationError,
  tooLarge
}
