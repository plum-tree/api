const get = require('lodash.get')
const log = require('./log')
const { getCookie } = require('./cookies')
const verifyToken = require('./verifyToken')

/**
 * Checks the user has a session (is logged in) and returns the JWT claims,
 * otherwise returns null.
 */
module.exports = (event) => {
  try {
    const token = getCookie(event, 'AuthToken')
    return verifyToken(token)
  } catch (err) {
    const message = get(err, 'message')
    if (message !== 'invalid signature' || message !== 'jwt expired') {
      log.error({ err })
    } else {
      log.info('User session invalid')
    }
    return null
  }
}
