const { Issuer, generators } = require('openid-client')
const get = require('lodash.get')
const getCallbackUri = require('./getCallbackUri')

/**
 * Utility to generate the Open ID Connect query params
 */
module.exports = async (returnDomain) => {
  const clientId = process.env.CLIENT_ID
  const clientSecret = process.env.CLIENT_SECRET
  const poolId = process.env.COGNITO_POOL_ID
  const codeVerifier = process.env.CODE_VERIFIER
  const redirectUri = getCallbackUri(returnDomain)
  const providerWellKnown = `https://cognito-idp.eu-west-1.amazonaws.com/${poolId}/.well-known/openid-configuration`

  const cognitoIssuer = await Issuer.discover(providerWellKnown)

  const client = new cognitoIssuer.Client({
    client_id: clientId,
    client_secret: clientSecret,
    redirect_uris: [redirectUri],
    response_types: ['code']
  })

  const codeChallenge = generators.codeChallenge(codeVerifier)

  const authorizationUrl = client.authorizationUrl({
    scope: 'openid email profile aws.cognito.signin.user.admin',
    code_challenge: codeChallenge,
    code_challenge_method: 'S256',
    // todo - use a proper nonce at some point
    nonce: 'foobar'
  })

  return get(authorizationUrl.split('?'), '[1]', '')
}
