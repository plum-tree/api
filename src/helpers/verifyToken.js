const jwkToPem = require('jwk-to-pem')
const jwt = require('jsonwebtoken')
const get = require('lodash.get')
const log = require('./log')

/**
 * Utility method to verify a AWS Cognito JWT token.
 * @param {string} token
 * @returns
 */
module.exports = (token) => {
  try {
    if (!token) {
      return null
    }

    // get the header from the token and determine the kid
    const decoded = jwt.decode(token, { complete: true })
    const kid = get(decoded, 'header.kid')

    // determine the jwk to use to generate a pem by the kid from the token
    // header
    // See https://cognito-idp.eu-west-1.amazonaws.com/<USER_POOL_ID>/.well-known/jwks.json
    const jwks = JSON.parse(process.env.JWKS)
    const jwk = jwks.keys.find(o => o.kid === kid)

    if (!jwk) {
      return null
    }

    // create the pem needed to verify the token
    const pem = jwkToPem(jwk)

    // verify the token with the pem
    return jwt.verify(token, pem, { algorithms: ['RS256'] })
  } catch (err) {
    // Session expired errors or no session are not worth polluting logs with
    // stack traces otherwise the error may be of interest
    if (err.name !== 'TokenExpiredError') {
      log.error({ err }, 'Failed to verify JWT')
    }

    return null
  }
}
