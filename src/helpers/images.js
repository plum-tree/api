const { S3Client, CopyObjectCommand, ListObjectsCommand, DeleteObjectsCommand } = require('@aws-sdk/client-s3')
const log = require('./log')
const get = require('lodash.get')

const client = new S3Client({ region: 'eu-west-1' })

const moveFromInputToProcessed = (image) => {
  log.info({ image }, 'Copying image from input to processed bucket')
  const command = new CopyObjectCommand({
    Bucket: process.env.UPLOAD_BUCKET_PROCESSED,
    CopySource: `/${process.env.UPLOAD_BUCKET_INPUT}/${image}`,
    Key: image
  })
  return client.send(command)
}

const deleteProcessedImage = async (image) => {
  log.info({ image }, 'Deleting image and any resized images from processed bucket')
  const parts = image.split('.')

  const listCommand = new ListObjectsCommand({
    Bucket: process.env.UPLOAD_BUCKET_PROCESSED,
    Prefix: parts[0]
  })
  const response = await client.send(listCommand)
  const Objects = response.Contents.map(c => ({ Key: c.Key }))

  const deleteCommand = new DeleteObjectsCommand({
    Bucket: process.env.UPLOAD_BUCKET_PROCESSED,
    Delete: {
      Objects
    }
  })
  return client.send(deleteCommand)
}

/**
 * Given a original tree and new tree object if the original tree cover image
 * does not match the new tree cover image this function deletes the cover from
 * the original in S3.
 * @param {Object} origTree
 * @param {Object} newTree
 */
const deleteOrphanedTreeCover = async (origTree, newTree) => {
  const origCover = get(origTree, 'cover', null)
  const newCover = get(newTree, 'cover', null)
  if (origCover && origCover !== newCover) {
    log.info({ origCover, newCover }, `Deleting old tree cover ${origCover}`)
    await deleteProcessedImage(origCover)
  }
}

/**
 * Given a list of original people and new people finds where images used in the
 * original people as avatars do not exist in the new people. Then those missing
 * avatars are deleted from S3.
 * @param {List(Object)} origPeople
 * @param {List(Object)} newPeople
 */
const deleteOrphanedPeopleAvatars = async (origPeople, newPeople) => {
  const origPeopleAvatars = origPeople
    .map(p => p.avatar)
    .filter(a => !!a)
  const newPeopleAvatars = newPeople
    .map(p => p.avatar)
    .filter(a => !!a)

  const imagesToDelete = origPeopleAvatars.filter(a => newPeopleAvatars.indexOf(a) === -1)
  log.info({ imagesToDelete }, `Deleting ${imagesToDelete.length} old people avatars`)
  for (let i = 0; i < imagesToDelete; i++) {
    await deleteProcessedImage(imagesToDelete[i])
  }
}

module.exports = {
  moveFromInputToProcessed,
  deleteProcessedImage,
  deleteOrphanedTreeCover,
  deleteOrphanedPeopleAvatars
}
